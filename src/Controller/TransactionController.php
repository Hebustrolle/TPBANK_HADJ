<?php

namespace App\Controller;


use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
class TransactionController extends AbstractController {

    
     /**
     * @Route("depot_retrait", methods={"PUT"})
     */
    function depotRetrait(Request $request)
    {
        if(isset($_SESSION['jwt'])){
            
        $identifiant = $request->get('identifiant');
        $montant = $request->get('montant');

        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $user = $userRepository->findByIdentifiant($request->get('identifiant'));
        
        if($user[0]->getMontant() + $montant >= 0 ){
            $em = $this->getDoctrine()->getManager();
            $userupdate = $em->getRepository(User::class)->findByIdentifiant($request->get('identifiant'));
            $userupdate[0]->setMontant($user[0]->getMontant() + $montant);
            $em->flush();
            $returnable = $user[0]->getMontant();

        }else{
            $returnable = "vous n'avez pas assez d'argent";
        }

      
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $jsonContent = $serializer->serialize($returnable, 'json');

        return new Response($jsonContent, Response::HTTP_CREATED);
    }else{
        return new Response("Connectez vous", Response::HTTP_OK, ['content-type' => 'application/json']);
    }
    }

    
    /**
     * @Route("virement_interne", methods={"PUT"})
     */
    function virementInterne(Request $request)
    {
        if(isset($_SESSION['jwt'])){
            
        $jwt = $_SESSION['jwt'];
        $payload = explode('.', $jwt);

        $user = base64_decode($payload[1]);

        $id = json_decode($user, true);


        if($request->get('compteSource') == $id['login']){
            
                $identifiant = $request->get('compteSource');
                $montant = $request->get('montant');
        
                $userRepository = $this->getDoctrine()->getRepository(User::class);
                $user = $userRepository->findByIdentifiant($identifiant);
                
                if($user[0]->getMontant() - $montant >= 0 ){
                    $em = $this->getDoctrine()->getManager();
                    $userupdate = $em->getRepository(User::class)->findByIdentifiant($identifiant);
                    $userupdate[0]->setMontant($user[0]->getMontant() - $montant);

                    $userDestinataireupdate = $em->getRepository(User::class)->findByIdentifiant($request->get('compteDestinataire'));
                    $userDestinataireupdate[0]->setMontant($user[0]->getMontant() + $montant);
                    $em->flush();
                    $returnable = $user[0]->getMontant();
        
                }else{
                    $returnable = "vous n'avez pas assez d'argent";
                }
                }else{
        $returnable = "Vous devez être connecté avec le compte débiteur";
    }

    }


    
      
    $encoders = [new JsonEncoder()];
    $normalizers = [new ObjectNormalizer()];

    $serializer = new Serializer($normalizers, $encoders);

    $jsonContent = $serializer->serialize($returnable, 'json');

    return new Response($jsonContent, Response::HTTP_CREATED);
}   

    }


?>